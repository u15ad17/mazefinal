import random


# Note: Using super() makes sure all the methods get called, but none of them twice.
# https://stackoverflow.com/questions/222877/what-does-super-do-in-python/33469090#33469090
# Example of the classic "diamond problem in OOP"
class Monster:
    """Blueprint for every Monster"""
    def __init__(self):
        """Set the initial values which are to be changed during the game"""
        self._coordX = None
        self._coordY = None
        self._luck = None
        self._health = None
        self._coins = None

    @property
    def coord_x(self):
        return self._coordX

    @property
    def coord_y(self):
        return self._coordY

    @property
    def coins(self):
        return self._coins

    @property
    def health(self):
        return self._health

    def qualities(self):
        """Joins a list of existing abilities"""
        # https://stackoverflow.com/questions/44118079/check-if-all-items-in-python-list-contain-substring
        return ' and '.join(filter(lambda lst: 'None' not in lst,
                                   [str(self._coins)+' coins', str(self._health)+' health points']))

    def coords(self):
        return f"[{self.coord_x}, {self.coord_y}]"

    """
    def decrease_health(self, hp):
        if self._health:
            hp -= self._health

    def decrease_money(self, amount):
        if self._coins:
            amount -= self._coins
    """

    @coord_x.setter
    def coord_x(self, value):
        self._coordX = value

    @coord_y.setter
    def coord_y(self, value):
        self._coordY = value

    @health.setter
    def health(self, value):
        self._health = value

    @coins.setter
    def coins(self, value):
        self._coins = value

    """
    # wrong alternative
    def set_coord_x(self, environment):
        self._coordX = add_monster(environment)[0]
        
    def set_coord_y(self, environment):
        self._coordY = add_monster(environment)[1]
    """


# ThiefMonster, FighterMonster are subclasses of the Monster Class
# GamerMonster inherits from ThiefMonster and FighterMonster
class ThiefMonster(Monster):
    def __init__(self):
        super().__init__()
        self._luck = random.randint(0, 100)
        self._coins = None

    def abilities(self):
        return f"({self._coins},{int(self._luck)}%)"

    def __str__(self):
        return "Thief Monster"


class FighterMonster(Monster):
    def __init__(self):
        super().__init__()
        self._luck = random.randint(0, 100)
        self._health = None

    def abilities(self):
        return f"({self._health},{int(self._luck)}%)"

    def __str__(self):
        return "Fighter Monster"


class GamerMonster(ThiefMonster, FighterMonster):
    def __init__(self):
        super().__init__()

    def abilities(self):
        return f"({self._coins},{int(self._health)})"

    def __str__(self):
        return "Gamer Monster"


if __name__ == '__main__':
    print("Testing....")
    for times in range(10):
        obj1 = ThiefMonster()
        print(obj1.luck)
        print(obj1)
