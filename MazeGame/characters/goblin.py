import random


# Note: Using super() makes sure all the methods get called, but none of them twice.
# https://stackoverflow.com/questions/222877/what-does-super-do-in-python/33469090#33469090
# Example of the classic "diamond problem in OOP"
class Goblin:
    """
    Parent class Goblin
    """
    def __init__(self):
        """Set the initial values which are to be changed during the game"""
        self._coordX = None
        self._coordY = None
        self._luck = None
        self._health = None
        self._coins = None

    @property
    def coord_x(self):
        return self._coordX

    @property
    def coord_y(self):
        return self._coordY

    @property
    def coins(self):
        return self._coins

    @property
    def health(self):
        return self._health

    def qualities(self):
        """Joins a list of existing abilities"""
        # https://stackoverflow.com/questions/44118079/check-if-all-items-in-python-list-contain-substring
        return ' and '.join(filter(lambda lst: 'None' not in lst,
                                   [str(self._coins)+' coins', str(self._health)+' health points']))

    def coords(self):
        return f"[{self.coord_x}, {self.coord_y}]"

    """
    def increase_health(self, hp):
        if self._health:
            hp += self._health

    def increase_money(self, amount):
        if self._coins:
            amount += self._coins
    """

    @coord_x.setter
    def coord_x(self, value):
        self._coordX = value

    @coord_y.setter
    def coord_y(self, value):
        self._coordY = value

    @health.setter
    def health(self, value):
        self._health = value

    @coins.setter
    def coins(self, value):
        self._coins = value


# WealthGoblin and HealthGoblin are child classes of the Goblin class
# GamerGoblin inherits from WealthGoblin and HealthGoblin
class WealthGoblin(Goblin):
    """
    WealthGoblin - child class of Goblin class
    """
    def __init__(self):
        super().__init__()
        self._luck = random.randint(1, 100)
        self._coins = None

    def abilities(self):
        return f"({self._coins},{int(self._luck)}%)"

    def __str__(self):
        return "Wealth Goblin"


class HealthGoblin(Goblin):
    """
    HealthGoblin - child class of Goblin class
    """
    def __init__(self):
        super().__init__()
        self._luck = random.randint(1, 100)
        self._health = None

    def abilities(self):
        return f"({self._health},{int(self._luck)}%)"

    def __str__(self):
        return "Health Goblin"


class GamerGoblin(WealthGoblin, HealthGoblin):
    def __init__(self):
        super().__init__()

    def abilities(self):
        return f"({self._coins},{int(self._health)})"

    def __str__(self):
        return "Gamer Goblin"


if __name__ == "__main__":
    print('Testing....')
    goblin1 = GamerGoblin()
    goblin2 = HealthGoblin()
    for i in range(5):
        print(f"Goblin1: {goblin1}")
        print(f"GAMERGoblin Qualities: {goblin1.qualities()}")
        print(f"HEALTHGoblin Qualities: {goblin2.qualities()}")
