from MazeGame.rock_paper_scissors.rps import RPS     # Importing the Rock-Paper-Scissors Game


class Hero:
    def __init__(self):
        self._coordX = None
        self._coordY = None
        self._health = 100  # health points the hero has.
        self._coins = 1000  # gold coins the hero has.

    # Creating property functions (Pythonic getters)
    # https://stackoverflow.com/questions/2627002/whats-the-pythonic-way-to-use-getters-and-setters
    @property
    def coord_x(self):
        return self._coordX

    @property
    def coord_y(self):
        return self._coordY

    @property
    def health(self):
        return self._health

    @property
    def coins(self):
        return self._coins

    # Creating Pythonic setter functions
    @health.setter
    def health(self, health):
        """Ensuring once the health is 0, it won't get down"""
        if health < 0:
            self._health = 0
        else:
            self._health = health

    @coins.setter
    def coins(self, coins):
        """Ensuring once the hero has no coins, monsters can't steal anymore."""
        if coins < 0:
            self._coins = 0
        else:
            self._coins = coins

    @coord_x.setter
    def coord_x(self, value):
        self._coordX = value

    @coord_y.setter
    def coord_y(self, value):
        self._coordY = value

    def hero_coords(self):
        return f"[{self.coord_x}, {self.coord_y}]"

    def increase_money(self, money):
        if money:
            self.coins += money

    def decrease_money(self, money):
        if money:
            self.coins -= money

    def increase_health(self, health):
        if health:
            self.health += health

    def decrement_health(self, health):
        if health:
            self.health -= health

    def __str__(self):
        return "Hero"

    def move(self, environment, move):
        """move in the maze, you need to press the enter key after keying in
        direction, and this works in the debug mode"""
        # Note: The function is controlled by recursion - until a valid move is made, make recursion calls
        moved = False       # indicates if a move has been made
        if environment.get_coord(self.coord_x, self.coord_y) == 3 or environment.get_coord(self.coord_x, self.coord_y) \
                == 23:
            environment.set_coord(self.coord_x, self.coord_y, 3)        # the monsters do not disappear
        else:
            environment.set_coord(self.coord_x, self.coord_y, 0)        # deleting the last position of the hero

        if move == "W":
            if environment.get_coord(self.coord_x - 1, self.coord_y) == 1:
                print("Heey, what are you doing...the hero can't move across #walls.")
                print("Use (A/S/D/W)\n")
                # self.move(environment, input().capitalize())    # recursion until a valid move is given
            else:
                self.coord_x -= 1
                # W was pressed
                print('---')
                print("'W' was pressed. The Hero moves up.")
                self.decrement_health(1)
                moved = True
        elif move == "S":
            if environment.get_coord(self.coord_x + 1, self.coord_y) == 1:
                print("Heey, what are you doing...the hero can't move across #walls.")
                print("Use (A/S/D/W)\n")
                # self.move(environment, input().capitalize())    # recursion until a valid move is given
            else:
                self.coord_x += 1
                # S was pressed
                print('---')
                print("'S' was pressed. The Hero moves down.")
                self.decrement_health(1)
                moved = True
        elif move == "A":
            if environment.get_coord(self.coord_x, self.coord_y - 1) == 1:
                print("Heey, what are you doing...the hero can't move across #walls.")
                print("Use (A/S/D/W)")
                # self.move(environment, input().capitalize())    # recursion until a valid move is given
            else:
                self.coord_y -= 1
                # A was pressed
                print("'A' was pressed. The Hero moves left.")
                self.decrement_health(1)
                moved = True
        elif move == "D":
            if environment.get_coord(self.coord_x, self.coord_y + 1) == 1:
                print("Heey, what are you doing...the hero can't move across #walls.")
                print("Use (A/S/D/W)\n")
                # self.move(environment, input().capitalize())    # recursion until a valid move is given
            else:
                self.coord_y += 1
                # D was pressed
                print("'D' was pressed. The Hero moves right.")
                self.decrement_health(1)
                moved = True
        else:
            print("Invalid command.")
            print("Try again (A/S/D/W)")
            self.move(environment, input().capitalize())

        if environment.get_coord(self.coord_x, self.coord_y) == 3:       # Monster is met
            environment.set_coord(self.coord_x, self.coord_y, 23)        # moves the hero to the new coordinates

        elif environment.get_coord(self.coord_x, self.coord_y) == 4:     # Goblin is met
            environment.set_coord(self.coord_x, self.coord_y, 24)        # moves the hero to the new coordinates
        else:
            environment.set_coord(self.coord_x, self.coord_y, 2)         # moves the hero to the new coordinates

        return moved

    def fight(self, creature):
        """Fight with creatures - this function interacts with the Rock Paper Scissors class"""
        battle = RPS(self, creature)
        winner = battle.interactive_rps()
        if winner is self:                          # If Hero wins a battle with a Goblin
            if "Goblin" in type(creature).__name__:
                self.increase_health(creature.health)
                self.increase_money(creature.coins)
                # creature.increase_health(self._health)
                # creature.increase_money(self._coins)
        elif winner is creature:                    # If Hero loses a battle with a Monster
            if "Monster" in type(creature).__name__:
                self.decrement_health(creature.health)
                self.decrease_money(creature.coins)
                # creature.decrease_health(self._health)
                # creature.decrease_money(self._coins)
        return winner
