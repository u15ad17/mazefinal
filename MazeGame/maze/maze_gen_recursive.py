# This code take ideas from http://arcade.academy/examples/maze_recursive.html

import random

# TODO: Delete Redundant stuff
SCREEN_WIDTH = 1000
SCREEN_HEIGHT = 700

TILE_EMPTY = 0
TILE_CRATE = 1
HERO = 2

# Maze must have an ODD number of rows and columns.
# Walls go on EVEN rows/columns.
# Openings go on ODD rows/columns
MAZE_HEIGHT = 51
MAZE_WIDTH = 51

walls = []
# This will store all the coordinates of all internal walls within the maze

used_coords = []     # cache memory
# Store the number of taken/occupied positions by the characters and brick walls(excluding the outside border walls)


def add_to_walls(value):
    # Making sure we keep track of the coordinates of the internal walls
    global walls
    if value not in walls:
        walls += [value]


def add_to_used(value):
    # Making sure we keep track of the used_coordinates in the maze so far
    global used_coords
    if value not in used_coords:
        used_coords += [value]


def create_empty_grid(height, width, default_value=TILE_EMPTY):
    """ Create an empty grid. """
    grid = []
    for row in range(height):
        grid.append([])
        for column in range(width):
            grid[row].append(default_value)
    return grid


def create_outside_walls(maze):
    """ Create outside border walls."""

    # Create left and right walls
    for row in range(len(maze)):
        maze[row][0] = TILE_CRATE
        maze[row][len(maze[row]) - 1] = TILE_CRATE

    # Create top and bottom walls
    for column in range(1, len(maze[0]) - 1):
        maze[0][column] = TILE_CRATE
        maze[len(maze) - 1][column] = TILE_CRATE


def make_maze_recursive_call(maze, top, bottom, left, right):
    global walls
    """
    Recursive function to divide up the maze in four sections
    and create three gaps.
    Walls can only go on even numbered rows/columns.
    Gaps can only go on odd numbered rows/columns.
    Maze must have an ODD number of rows and columns.
    ---- top=maze_height - 1, bottom=0, left=0, right=maze_width - 1 ---      are the given parameters
    """

    # Figure out where to divide horizontally
    start_range = bottom + 2
    end_range = top - 1
    y = random.randrange(start_range, end_range, 2)

    # Do the division
    for column in range(left + 1, right):
        add_to_walls([y, column])               # making sure the wall coordinates will be added to the walls container
        maze[y][column] = TILE_CRATE

    # Figure out where to divide vertically
    start_range = left + 2
    end_range = right - 1
    x = random.randrange(start_range, end_range, 2)

    # Do the division
    for row in range(bottom + 1, top):
        add_to_walls([row, x])
        maze[row][x] = TILE_CRATE            # making sure the wall coordinates will be added to the walls container

    # Now we'll make a gap on 3 of the 4 walls.
    # Figure out which wall does NOT get a gap.
    wall = random.randrange(4)
    if wall != 0:
        gap = random.randrange(left + 1, x, 2)
        maze[y][gap] = TILE_EMPTY
        walls.remove([y, gap])               # making sure the wall coordinates will be removed from the walls container

    if wall != 1:
        gap = random.randrange(x + 1, right, 2)
        maze[y][gap] = TILE_EMPTY
        walls.remove([y, gap])               # making sure the wall coordinates will be removed from the walls container

    if wall != 2:
        gap = random.randrange(bottom + 1, y, 2)
        maze[gap][x] = TILE_EMPTY
        walls.remove([gap, x])               # making sure the wall coordinates will be removed from the walls container

    if wall != 3:
        gap = random.randrange(y + 1, top, 2)
        maze[gap][x] = TILE_EMPTY
        walls.remove([gap, x])               # making sure the wall coordinates will be removed from the walls container

    # If there's enough space, to a recursive call.
    if top > y + 3 and x > left + 3:
        make_maze_recursive_call(maze, top, y, left, x)

    if top > y + 3 and x + 3 < right:
        make_maze_recursive_call(maze, top, y, x, right)

    if bottom + 3 < y and x + 3 < right:
        make_maze_recursive_call(maze, y, bottom, x, right)

    if bottom + 3 < y and x > left + 3:
        make_maze_recursive_call(maze, y, bottom, left, x)


def avoid_overriding(coords, used, height, width):
    # -------- avoiding the case of overriding existing walls or characters ---------     TODO: Try to optimize it
    while coords in used:
        coords = [random.randrange(1, height - 1), random.randrange(1, width - 1)]
    # -------- NOTE: coords will avoid overriding outside border walls in any case -------
    return coords


def add_hero(maze, height, width):
    # Adds a hero to the maze
    global used_coords
    used_coords = walls.copy()
    hero = [random.randrange(1, height - 1), random.randrange(1, width - 1)]
    hero_coords = avoid_overriding(hero, used_coords, height, width)
    maze[hero_coords[0]][hero_coords[1]] = 2
    add_to_used(hero_coords)
    return hero_coords


def add_monster(maze):
    # Adds a monster to the maze
    global used_coords
    monster = [random.randrange(1, maze.height-1), random.randrange(1, maze.width-1)]
    cord_monster = avoid_overriding(monster, used_coords, maze.height, maze.width)
    maze.set_coord(cord_monster[0], cord_monster[1], 3)
    add_to_used(cord_monster)
    return cord_monster


def add_goblin(maze):
    # Adds a goblin to the maze
    global used_coords
    goblin = [random.randrange(1, maze.height-1), random.randrange(1, maze.width-1)]
    cord_goblin = avoid_overriding(goblin, used_coords, maze.height, maze.width)
    maze.set_coord(cord_goblin[0], cord_goblin[1], 4)
    add_to_used(cord_goblin)
    return cord_goblin


"""
# Personal Use: 
# This function adds the hero, the monsters and the goblins to the maze (It adds just their initials - H/M/G)
def maze_hero_monster_goblin(maze, height, width):
    hero = [random.randrange(1, height-1), random.randrange(1, width-1)]
    hero_coords = avoid_overriding(hero, used_coords, height, width)
    used_coords.append(hero_coords)
    maze[hero_coords[0]][hero_coords[1]] = 2

    monster_coords = []     # keeping track of the monsters coordinates in the maze
    goblin_coords = []      # keeping track of the goblin coordinates in the maze
    for i in range(5):
        # Adding the 5 monsters to the maze
        monster = [random.randrange(1, height-1), random.randrange(1, width-1)]
        cord_monster = avoid_overriding(monster, used_coords, height, width)
        used_coords.append(cord_monster)
        monster_coords.append(cord_monster)
        maze[cord_monster[0]][cord_monster[1]] = 3

        # Adding the 5 goblins to the maze
        goblin = [random.randrange(1, height-1), random.randrange(1, width-1)]
        cord_goblin = avoid_overriding(goblin, used_coords, height, width)
        used_coords.append(cord_goblin)
        goblin_coords.append(cord_goblin)
        maze[cord_goblin[0]][cord_goblin[1]] = 4

    # -----------------------------------------------------------------------------------
    # Some Informative stuff here:
    # print(len(used_coords))
    '''
    # Printing the number of taken/occupied positions by the characters and brick walls 
    # (excluding the the outside border walls)
    '''
    # print(used_coords)  # prints the coordinates of all fore-mentioned used coordinates
    # -------------------------------------------------------------------------------------

    return hero_coords, monster_coords, goblin_coords
    # Making sure we keep track of the characters coordinates in the maze
"""


def make_maze_recursion(maze_height, maze_width):
    """ Make the maze by recursively splitting it into four rooms."""
    # Note: The function initializes the map and the hero inside, returning the map and hero's coords
    maze = create_empty_grid(maze_height, maze_width)
    # Fill in the outside walls
    create_outside_walls(maze)

    # Start the recursive process
    make_maze_recursive_call(maze, maze_height - 1, 0, 0, maze_width - 1)
    hero = add_hero(maze, maze_height, maze_width)

    # Some previous attempts here:
    # hero, monster, goblin = maze_hero_monster_goblin(maze, maze_height, maze_width)
    # return maze, hero, monster, goblin

    return maze, hero


def print_maze(maze, wall="#", space="-", hero_char='H', monster_char='M', goblin_char='G'):
    """Prints out the maze in the terminal"""
    for row in maze:
        row_str = str(row)
        row_str = row_str.replace("0", space)         # replace the space character
        row_str = row_str.replace("1", wall)          # replace the wall character
        row_str = row_str.replace("2", hero_char)     # replace the hero character
        row_str = row_str.replace("3", monster_char)  # replace the monster character
        row_str = row_str.replace("4", goblin_char)   # replace the goblin char
        print("".join(row_str))


if __name__ == "__main__":
    # Some information about the maze is printed here
    card, hero_coord = make_maze_recursion(7, 7)            # TODO: Change to 17
    print_maze(card, "#", "-")
    print(card, "#", "-")

    # Printing informative stuff
    # -----------------------------------------------------------------------------------
    print()
    print(f"Hero coords: {hero_coord}")

    print(f"The number of the internal wall bricks is: {len(walls)}",
          end=' ----> ')
    print(walls)        # prints the coordinates of the internal wall bricks only
    print(f"The number of taken/occupied coordinates(by characters and internal bricks only)is: {len(used_coords)}",
          end=' ----> ')
    print(used_coords)  # prints the coordinates of all fore-mentioned used coordinates
    # -------------------------------------------------------------------------------------
