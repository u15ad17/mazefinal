from MazeGame.characters.hero import Hero
from MazeGame.characters.goblin import GamerGoblin, WealthGoblin, HealthGoblin
from MazeGame.characters.monster import GamerMonster, ThiefMonster, FighterMonster
from MazeGame.maze.maze_gen_recursive import make_maze_recursion, print_maze, add_monster, add_goblin
from copy import deepcopy
import random
import pickle           # used for saving/loading
import traceback        # used for saving


WALL_CHAR = "#"
SPACE_CHAR = "-"
HERO_CHAR = "H"
MONSTER_CHAR = 'M'
GOBLIN_CHAR = 'G'


class _Environment:
    """Environment includes Maze+Monster+Goblin"""
    def __init__(self, maze):
        self._environment = deepcopy(maze)
        self._height = len(maze)
        self._width = len(maze[0])

    @property
    def height(self):
        return self._height

    @property
    def width(self):
        return self._width

    def set_coord(self, x, y, val):
        self._environment[x][y] = val

    def get_coord(self, x, y):
        return self._environment[x][y]

    def print_environment(self):
        """Print out the environment in the terminal"""
        print_maze(self._environment, WALL_CHAR, SPACE_CHAR, HERO_CHAR, MONSTER_CHAR, GOBLIN_CHAR)


class Game:
    """
    The Game class contains the maze, the hero, the monsters, the goblins and their coordinates in the maze.
    """
    _count = 0

    # container of all classes except from the hero - will be used to generate 4 random characters
    characters = [["GamerGoblin()", "WealthGoblin()", "HealthGoblin()"], ["ThiefMonster()", "FighterMonster()",
                  "GamerMonster()"]]        # Note that the Goblins are divided from the Monsters

    def __init__(self):
        # ------ The following code is used for the save-load part ----------------------------------------
        # http://stackoverflow.com/questions/1690400/getting-an-instance-name-inside-class-init
        (filename, line_number, function_name, text) = traceback.extract_stack()[-2]
        def_name = text[:text.find('=')].strip()
        self.name = def_name   # set name from variable name.
        # -------------------------------------------------------------------------------------------------

        self.myHero = Hero()
        self.maze, self._hero_coords = make_maze_recursion(17, 17)    # initialize the map with the hero symbol inside
        self.MyEnvironment = _Environment(self.maze)                  # initial environment is the maze itself

        # Instance variable, which will store all creature objects (I decided it to set it as an instance var instead of
        # a class variable, so we can use it when the instance of the Game class is saved and then loaded
        self.all_characters = []
        self.dif_level = None      # stores the level of difficulty

        # All three types of monsters and all three types of goblins should be generated.
        self.wealth_goblin = WealthGoblin()
        self.all_characters += [self.wealth_goblin]

        self.health_goblin = HealthGoblin()
        self.all_characters += [self.health_goblin]

        self.gamer_goblin = GamerGoblin()
        self.all_characters += [self.gamer_goblin]

        self.thief_monster = ThiefMonster()
        self.all_characters += [self.thief_monster]

        self.fighter_monster = FighterMonster()
        self.all_characters += [self.fighter_monster]

        self.gamer_monster = GamerMonster()
        self.all_characters += [self.gamer_monster]

        # The remaining 2 monsters and 2 goblins will be generated at random
        self.goblin4 = eval(random.choice(Game.characters[0]))
        self.all_characters += [self.goblin4]

        self.goblin5 = eval(random.choice(Game.characters[0]))
        self.all_characters += [self.goblin5]

        self.monster4 = eval(random.choice(Game.characters[1]))
        self.all_characters += [self.monster4]

        self.monster5 = eval(random.choice(Game.characters[1]))
        self.all_characters += [self.monster5]
        # idea for eval taken from here:
        # https://stackoverflow.com/questions/4471884/how-to-turn-a-string-formula-into-a-real-formula

        # monsters will contain all the monsters, so we can know when all monsters are met
        self.monsters = [monster for monster in self.all_characters if 'Monster' in type(monster).__name__]
        self._count = 0

    def set_dif_level(self, value):
        self.dif_level = value

    def initialize_characters(self):
        """The function replaces the existing Hero symbol in the maze with an instance of the Hero class,
        so we can use its attributes and behaviours.
        The function also initialize the monsters and the goblins in the maze.
        """
        self.myHero.coord_x, self.myHero.coord_y = self._hero_coords
        # set the coordinates of the hero object
        self.MyEnvironment.set_coord(self.myHero.coord_x, self.myHero.coord_y, 2)
        # replaces the 'H' symbol with the hero object

        for creature in self.all_characters:
            # https://stackoverflow.com/questions/510972/getting-the-class-name-of-an-instance Reference
            if 'Monster' in type(creature).__name__:
                creature.coord_x, creature.coord_y = add_monster(self.MyEnvironment)
            elif 'Goblin' in type(creature).__name__:
                creature.coord_x, creature.coord_y = add_goblin(self.MyEnvironment)

    def initialize_game(self):
        """The Function prints the map with all the characters inside and prints a report on initialisation
        The Game Difficulty Level is also decided within this function.
        """
        diff_levels = ['1', '2', '3', '4']
        level = input("""Choose difficulty level:
1.Easy       (Press 1)
2.Medium     (Press 2)
3.Hard       (Press 3)
4.Very Hard  (Press 4)
""")
        while level not in diff_levels:
            print("Incorrect input.")
            level = input("""Choose difficulty level:
1.Easy       (Press 1)
2.Medium     (Press 2)
3.Hard       (Press 3)
4.Very Hard  (Press 4)
""")
        if level == '1':      # Easy
            level = "easy"
            for creature in self.all_characters:
                if 'Monster' in type(creature).__name__:
                    creature.health = random.randint(1, 25)
                    creature.coins = random.randint(1, 250)
                elif 'Goblin' in type(creature).__name__:
                    creature.health = random.randint(75, 100)
                    creature.coins = random.randint(750, 1000)
        elif level == '2':    # Medium
            level = "medium"
            for creature in self.all_characters:
                if 'Monster' in type(creature).__name__:
                    creature.health = random.randint(25, 50)
                    creature.coins = random.randint(250, 500)
                elif 'Goblin' in type(creature).__name__:
                    creature.health = random.randint(50, 75)
                    creature.coins = random.randint(500, 750)
        elif level == '3':    # Hard
            level = "hard"
            for creature in self.all_characters:
                if 'Monster' in type(creature).__name__:
                    creature.health = random.randint(50, 75)
                    creature.coins = random.randint(50, 750)
                elif 'Goblin' in type(creature).__name__:
                    creature.health = random.randint(25, 50)
                    creature.coins = random.randint(250, 500)
        elif level == '4':    # Very Hard
            level = "veryhard"
            for creature in self.all_characters:
                if 'Monster' in type(creature).__name__:
                    creature.health = random.randint(75, 100)
                    creature.coins = random.randint(750, 1000)
                elif 'Goblin' in type(creature).__name__:
                    creature.health = random.randint(1, 25)
                    creature.coins = random.randint(1, 250)

        self.initialize_characters()
        print("Initial map:")
        self.MyEnvironment.print_environment()
        self.report()
        self.set_dif_level(level)

    def league_table(self, name, dif_level):
        # dif_level holds a string of level difficulty (easy, medium, hard..)
        with open('league_table.txt', 'a') as file:
            file.write(str(f"{name} {self.myHero.coins} {dif_level}\n"))

    def ranking(self):
        # TODO Optimize it
        """The Function displays the rank of top (max 10) players at the corresponding difficulty level"""
        easy_scores = {}
        medium_scores = {}
        hard_scores = {}
        very_hard_scores = {}
        with open('league_table.txt', 'r') as file:
            for line in file:
                data = line.rstrip().split()  # results in a list
                name = data[0]
                score = data[1]
                level = data[2]
                if level == "easy":
                    easy_scores[name] = score
                elif level == "medium":
                    medium_scores[name] = score
                elif level == "hard":
                    hard_scores[name] = score
                elif level == "veryhard":
                    very_hard_scores[name] = score
                else:
                    print("Error")
        # https://stackoverflow.com/questions/21667752/sort-dictionary-by-the-int-value-of-the-value
        rank = 1
        if self.dif_level == "easy":
            if len(easy_scores) >= 10:
                print(f"The top 10 players of the easy level are:")
            else:
                print(f"The top {len(easy_scores)} players of the easy level are:")
            for k, v in sorted(easy_scores.items(), key=lambda x: int(x[1]), reverse=True):
                print(f"{rank}.{k}: {v} coins")  # Displays the rank, name and score of the player
                if rank < 10:
                    rank += 1
                else:
                    return
        elif self.dif_level == "medium":
            if len(medium_scores) >= 10:
                print(f"The top 10 players of the medium level are:")
            else:
                print(f"The top {len(medium_scores)} players of the medium level are:")
            for k, v in sorted(medium_scores.items(), key=lambda x: int(x[1]), reverse=True):
                print(f"{rank}.{k}: {v} coins")
                if rank < 10:
                    rank += 1
                else:
                    return
        elif self.dif_level == "hard":
            if len(hard_scores) >= 10:
                print(f"The top 10 players of the hard level are:")
            else:
                print(f"The top {len(hard_scores)} players of the hard level are:")
            for k, v in sorted(hard_scores.items(), key=lambda x: int(x[1]), reverse=True):
                print(f"{rank}.{k}: {v} coins")
                if rank < 10:
                    rank += 1
                else:
                    return
        elif self.dif_level == "veryhard":
            if len(very_hard_scores) >= 10:
                print(f"The top 10 players of the very hard level are:")
            else:
                print(f"The top {len(very_hard_scores)} players of the very hard level are:")
            for k, v in sorted(very_hard_scores.items(), key=lambda x: int(x[1]), reverse=True):
                print(f"{rank}.{k}: {v} coins")
                if rank < 10:
                    rank += 1
                else:
                    return

    def save_game(self):
        """Saving the game"""
        # Source: https://stackoverflow.com/questions/2345151/how-to-save-read-class-wholly-in-python
        with open(self.name+'.txt', 'wb') as save_file:
            save_file.write(pickle.dumps(self.__dict__))

    def load_game(self):
        """Loading self.name.txt"""
        with open(self.name+'.txt', 'rb') as load_file:
            loading = load_file.read()
            self.__dict__ = pickle.loads(loading)

    @staticmethod
    def display_menu():
        """This function informs the player about the possible moves"""
        print("Press 'W' to move your Hero up.")
        print("Press 'S' to move your Hero down.")
        print("Press 'A' to move your Hero left.")
        print("Press 'D' to move your Hero right.")
        print("Press 'M' to see the current map and the coords of the characters.")
        print("Press 'Z' to save the game.")

    def report(self):
        # random.shuffle(self.all_characters)      # shuffling the list
        """Informs player about the Monsters and the Goblins - types, abilities, positions"""
        print()
        print("Individual types, abilities and positions: ")
        for creature in reversed(self.all_characters):
            print(f"{creature}{creature.abilities()}: {creature.coords()}")

        # print(f"{'  '.join(map(str, cls.all_characters))}:{creature.coords() for creature in cls.all_characters}",
        # end='  ')

    def display_positions(self):
        # Printing everyone's position in the maze (by requirements)
        # Here I decided to print all the monster coords exhaustively instead of using a for loop in the class
        # all_characters container, because monsters are not getting removed once met by the hero

        print("Everyone's position in the map: ")
        print(f"{self.myHero}:{self.myHero.hero_coords()}")
        print(f"{self.thief_monster}:{self.thief_monster.coords()}", end=' ')
        print(f"{self.fighter_monster}:{self.fighter_monster.coords()}", end=' ')
        print(f"{self.gamer_monster}:{self.gamer_monster.coords()}", end=' ')
        print(f"{self.monster4}:{self.monster4.coords()}", end=' ')  # random monster 1
        print(f"{self.monster5}:{self.monster5.coords()}")           # random monster 2

        for goblin in self.all_characters:
            if 'Goblin' in type(goblin).__name__:
                print(f"{goblin}:{goblin.coords()}", end=' ')
        print()

    def game_loop(self, move_hero):
        """This function asks the user for his decision and displays information according to the user input.
        Note: If the user presses A/S/D/W the hero will be moved (the function will call the hero move behaviour from
        the Hero class), otherwise recursive calls controls the function until a direction is chosen.
        """

        user_input = input().capitalize()
        # Recursion controls the input, making recursive calls until a direction is chosen
        if user_input not in move_hero:
            if user_input == "H":
                self.display_menu()
                print("Choose your move: ", end='')
            elif user_input == "M":
                self.MyEnvironment.print_environment()
                self.display_positions()                # Displays everyone's positions
                print("\nChoose your next move(A/S/D/W/H): ", end='')
            elif user_input == 'Z':
                self.save_game()
                print('Game Saved.')
            else:
                print("Invalid command.")
            self.game_loop(move_hero)          # Make recursive calls until the next hero move is chosen

        else:
            moved = self.myHero.move(self.MyEnvironment, user_input)
            if moved:
                self.MyEnvironment.print_environment()
            print(f"Hero is at: {self.myHero.hero_coords()} with {self.myHero.health} health points")
            return          # Breaks out of the whole function -> Base Case when hero is moved

    def play(self):
        print("""=======================================================================================================
Welcome to the land of monsters and goblins, time of your life!
Your task is to survive in this wild, wild land. Remember: Goblins are your friends, while Monsters are your enemies! 
Every move you make, costs 1 health point. The rest is hidden in the land.
Choose your moves wisely! 

H = You(The Hero)   
M = Monster
G = Goblin
""")
        print("If you want to reload your last saved game, please press 'L' and then 'Enter'! "
              "Otherwise press any key to start the game. ")
        reload = input("L/start(S): ").upper()
        if reload == 'L':       # Loading last saved game
            try:
                self.load_game()
                print("Last saved map:")
                self.MyEnvironment.print_environment()
                self.report()
            except EOFError:
                print('----------')
                print("No previous records of the game are saved.")
                print("Starting a new game....")
                self.initialize_game()
        else:
            print("New game starts...")
            self.initialize_game()
        print("====================================================================================================")

        while self.myHero.health > 0:
            print("Press 'H' to see the available operations(keys).")
            self.game_loop(["A", "S", "D", "W"])
            hero_place = self.MyEnvironment.get_coord(self.myHero.coord_x, self.myHero.coord_y)
            # ---------------------- Hero meets a monster or a goblin ------------------
            if hero_place > 20:     # If hero meets a creature, example 23 means Hero and Monster
                for creature in self.all_characters:
                    if self.myHero.hero_coords() == creature.coords():
                        print(f"The Hero meets a {creature} with the ability of {creature.abilities()} "
                              f"at coordinate {creature.coords()}.")
                        if 'Goblin' in type(creature).__name__:
                            self.all_characters.remove(creature)       # remove goblin
                            print(f"A game will be played and if the hero wins the game, they will get "
                                  f"{creature.qualities()} from the goblin.")
                            winner = self.myHero.fight(creature)    # who the winner is?
                            if winner == self.myHero:
                                print(f"The Hero won the battle with the {creature} at coordinate "
                                      f"{creature.coords()}, winning {creature.qualities()}.")
                            elif winner == creature:
                                print(f"The Hero lost the battle with the {creature} "
                                      f"at coordinate {creature.coords()}.")

                        elif 'Monster' in type(creature).__name__:
                            print(f"A game will be played and if the hero loses the game, the monster will take "
                                  f"{creature.qualities()} from the hero. Be careful!")
                            winner = self.myHero.fight(creature)
                            if winner == self.myHero:
                                print(f"The Hero won the battle with the {creature} "
                                      f"at coordinate {creature.coords()}.")
                            elif winner == creature:
                                print(f"The Hero lost the battle with the {creature} at coordinate "
                                      f"{creature.coords()}, {creature.qualities()} were lost.")
                            if creature in self.monsters:
                                """Removing the visited monsters from the monsters list"""
                                self.monsters.remove(creature)
                        else:
                            print("An error has occurred")      # Covering errors

                        print(f"Hero's current status is:(health:{self.myHero.health}, coins:{self.myHero.coins}).")

                        # Checking if the hero wins by visiting all the monsters
                        if not self.monsters and self.myHero.health > 0:
                            """In case the Hero visits all monsters and survives, the game ends successfully"""
                            print("WELL DONE!\n"
                                  "***You visited every monster and survived. The game is successful.***")
                            name = input("Please, enter your username: ").replace(' ', '')
                            self.league_table(name, self.dif_level)
                            self.ranking()      # Displays the ranking for the appropriate level
                            return
                # -------------------- Appropriate Messages are displayed ---------------------

            self._count += 1
            print("--------------------------------------------------------------------------------------", self._count,
                  '\n')
            if self.myHero.health <= 0:
                    print("The hero has been killed. The game is unsuccessful.")


if __name__ == "__main__":
    myGame = Game()
    myGame.play()
