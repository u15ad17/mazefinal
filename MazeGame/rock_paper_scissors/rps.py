import random
import time


class RpsPlayer:
    """Class player - contains the name and the chosen hand to play in the game"""
    def __init__(self, player):
        self.name = player
        self._player_hand = None

    @property
    def player_hand(self):
        return self._player_hand

    @player_hand.setter
    def player_hand(self, value):
        self._player_hand = value


class RPS:
    def __init__(self, player1, player2):       # 2 Players take part in the game
        self.player1 = RpsPlayer(player1)
        self.player2 = RpsPlayer(player2)
        self._winner = None

        # Print out a message so we know when the constructor has run and the game has been started
        print("""----------------------------------
Welcome to the Rock-Paper-Scissors Game:""")

    @staticmethod
    def hand_picked(player):
        if player.player_hand in [1, 'I', "SCISSORS"]:
            print(f"{player.name} picks scissors")
        elif player.player_hand in [2, 'R', "ROCK"]:
            print(f"{player.name} picks rock")
        elif player.player_hand in [3, 'P', "PAPER"]:
            print(f"{player.name} picks paper")

    def interactive_rps(self):
        legal_hands = ['R', "ROCK", 'P', "PAPER", 'I', "SCISSORS"]
        self.player1.player_hand = input("Pick R-rock, P-paper or I-scissors: ").upper()

        while self.player1.player_hand not in legal_hands:      # Checking for a proper input
            print("Invalid input, try again.")
            self.player1.player_hand = input("Pick R-rock, P-paper or I-scissors: ").upper()
        self.hand_picked(self.player1)          # Informs about the picked hand

        self.player2.player_hand = (random.randint(1, 3))
        self.hand_picked(self.player2)              # Informs about the picked hand

        # Possible outcomes of the game
        if self.player1.player_hand in legal_hands[0:2] and self.player2.player_hand == 1:
            self._winner = self.player1.name
        elif self.player1.player_hand in legal_hands[0:2] and self.player2.player_hand == 3:
            self._winner = self.player2.name
        elif self.player1.player_hand in legal_hands[2:4] and self.player2.player_hand == 1:
            self._winner = self.player2.name
        elif self.player1.player_hand in legal_hands[2:4] and self.player2.player_hand == 2:
            self._winner = self.player1.name
        elif self.player1.player_hand in legal_hands[4:6] and self.player2.player_hand == 2:
            self._winner = self.player2.name
        elif self.player1.player_hand in legal_hands[4:6] and self.player2.player_hand == 3:
            self._winner = self.player1.name

        if self._winner is None:
            print("GAME TIE. Another round is to be played...\n")
            self.interactive_rps()
        else:
            print('----------------------------------')     # We know the battle has ended at this point

        return self._winner

    def automatic_rps(self):
        self.player1.player_hand = (random.randint(1, 3))
        self.player2.player_hand = (random.randint(1, 3))

        self.hand_picked(self.player1)      # Informs about the picked hand
        self.hand_picked(self.player2)      # Informs about the picked hand

        if self.player1.player_hand == 2 and self.player2.player_hand == 1:
            self._winner = self.player1.name
        elif self.player1.player_hand == 2 and self.player2.player_hand == 3:
            self._winner = self.player2.name
        elif self.player1.player_hand == 3 and self.player2.player_hand == 1:
            self._winner = self.player2.name
        elif self.player1.player_hand == 3 and self.player2.player_hand == 2:
            self._winner = self.player1.name
        elif self.player1.player_hand == 1 and self.player2.player_hand == 2:
            self._winner = self.player2.name
        elif self.player1.player_hand == 1 and self.player2.player_hand == 3:
            self._winner = self.player1.name

        if self._winner is None:
            print("Game tie. Another round is to be played...\n")
            self.automatic_rps()
        else:
            print('----------------------------------')     # We know the battle has ended at this point

        return self._winner


if __name__ == '__main__':
    while True:
        # "Testing...."
        game = RPS('player1', 'player2')
        game.interactive_rps()
        time.sleep(1)
        input('Press enter to start again')
